#
#
# MCSim - Monte Carlo simulation of lattice polymers and proteins
#
# Copyright (C) 2006 - 2019 Thomas Wuest <monte.carlo.coder@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#

.SUFFIXES:
.SUFFIXES: .C .o


#CFLAGS = -U_FORTIFY_SOURCE -g -pg -Wall -Wextra -pedantic
CFLAGS = -U_FORTIFY_SOURCE -O3 -mtune=generic


# GNU Scientific Library (GSL) support:
GSL_C := $(shell gsl-config --cflags)
GSL_L := $(shell gsl-config --libs)


# Object files:
OBJS = Vector.o      \
       Field.o       \
       Globals.o     \
       Histogram.o   \
       MonteCarlo.o  \
       HPModel.o     \
       Main.o


# Target file:
TARGET = MCSim

.C.o:
	$(CXX) $(CFLAGS) $(GSL_C) -c $*.C

$(TARGET): $(OBJS)
	$(CXX) $(CFLAGS) -o $@ $(OBJS) $(GSL_L)


Vector.o: Vector.C Vector.H
Field.o: Field.C Field.H
Globals.o: Globals.C Globals.H
Histogram.o: Histogram.C Histogram.H
MonteCarlo.o: MonteCarlo.C Globals.H Histogram.H Model.H MonteCarlo.H
HPModel.o: HPModel.C Vector.H Field.H Globals.H Histogram.H Model.H HPModel.H
Main.o: Main.C Globals.H MonteCarlo.H Histogram.H Model.H HPModel.H


cleanup:
	rm -f *.o $(TARGET)
